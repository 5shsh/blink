import paho.mqtt.client as mqtt
import requests
import os
import os.path

from datetime import datetime
import time

import shutil
import json

import pprint

MQTT_HOST = os.environ['MQTT_HOST']

class Blink:

    login_url = 'https://rest-prod.immedia-semi.com/api/v4/account/login'
    homescreen_url = 'https://rest-#region#.immedia-semi.com/homescreen'
    network_url = 'https://rest.#region#.immedia-semi.com/networks'
    trigger_video_url = 'https://rest.#region#.immedia-semi.com/network/#network_id#/camera/#camera_id#/clip'
    video_list_url = 'https://rest.#region#.immedia-semi.com/api/v1/accounts/#account_id#/media/changed?'
    video_download_url = 'https://rest.#region#.immedia-semi.com'

    authToken = False
    region = False
    account_id = False
    client_id = False
    camera_id = False
    network_id = False

    username = False
    password = False

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.auth(username, password)

    def auth(self, username, password):
        headers = {
            'Host': 'prod.immedia-semi.com',
            'Content-Type': 'application/json'
        }

        payload = {
            "email": username,
            "password": password,
            "client_specifier": "iPhone 9.2 | 2.2 | 222"
        }

        res = requests.post(self.login_url, headers=headers, data=json.dumps(payload)).json()

        self.authToken = res["authtoken"]["authtoken"]
        self.region = res["region"]["tier"]
        self.client_id = res["client"]["id"]
        self.account_id = res["account"]["id"]
        print("authToken: " + self.authToken + ", region: " + self.region + ", client_id: " + str(self.client_id) + ", account_id: " + str(self.account_id))

    def get_auth_token(self):
        return {
            'Host': 'prod.immedia-semi.com',
            'TOKEN_AUTH': self.authToken
        }

    def homescreen(self):
        url = self.homescreen_url.replace('#region#', self.region)
        res = requests.get(url, headers=self.get_auth_token()).json()

        self.camera_id = res["devices"][0]["device_id"]
        print("camera_id: " + str(self.camera_id))


    def network(self):
        url = self.network_url.replace('#region#', self.region)

        res = requests.get(url, headers=self.get_auth_token()).json()

        self.network_id = res["networks"][0]["id"]
        print("network_id: " + str(self.network_id))

    def trigger_video(self):
        print('trigger video recording')

        url = self.trigger_video_url.replace("#region#", self.region).replace("#network_id#", str(self.network_id)).replace("#camera_id#", str(self.camera_id))
        res = requests.post(url, headers=self.get_auth_token()).json()
        pprint.pprint(res)
        if ("created_at" not in res):
            # try to re-auth and trigger video another time
            self.auth(self.username, self.password)
            res = requests.post(url, headers=self.get_auth_token()).json()
            
        pprint.pprint(res)

        print("trigger_video created_at: " + res["created_at"])

    def download_video(self):
        print('download video')
        
        url = self.video_list_url.replace("#region#", self.region).replace("#account_id#", str(self.account_id))
        param = "since=" + datetime.now().strftime("%Y-%m-%d") + "T00:00:00+0000"

        res = requests.get(url + param, headers=self.get_auth_token()).json()

        media_path = res["media"][0]["media"]
        print("download_video media_path: " + media_path)

        url = self.video_download_url.replace('#region#', self.region) + media_path

        res = requests.get(url, headers=self.get_auth_token(), stream=True)
        with open("/tmp/tmp-download", 'wb') as out_file:
            shutil.copyfileobj(res.raw, out_file)
        
        now = datetime.now()
        path = '/video/' + now.strftime("%Y-%m-%d")
        if not os.path.exists(path):
            os.mkdir(path)
        filename = path + '/' + now.strftime("%Y-%m-%d_%H-%M-%S") + '.mp4'
        shutil.move("/tmp/tmp-download", filename)
        print("download_video video_path: " + filename)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("/blink/record")

def on_message(client, userdata, msg):
    global b

    print(msg.topic + " " + str(msg.payload))
    if (msg.topic == "/blink/record"):
        print("message received: /blink/record")
        b.trigger_video()
        print("waiting for recording")
        time.sleep(15)
        b.download_video()



if __name__ == "__main__":
    
    username = os.environ['BLINK_USERNAME']
    password = os.environ['BLINK_PASSWORD']

    global b

    b = Blink(username, password)
    b.homescreen()
    b.network()

    client = mqtt.Client('blink')
    client.connect(MQTT_HOST, 1883, 60)
    client.on_connect = on_connect
    client.on_message = on_message

    client.loop_forever()
