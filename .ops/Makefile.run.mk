docker_run:
	docker run -d --name=mosquitto_test_run -p 1883:1883 homesmarthome/mosquitto:latest
	docker run -d \
	  --name=blink_test_run \
	  --link mosquitto_test_run:mosquitto \
	  -v $(PWD)/.env:/env \
	  -v $(PWD)/tmp:/video \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep blink_test_run

docker_stop:
	docker rm -f blink_test_run 2> /dev/null; true
	docker rm -f mosquitto_test_run 2> /dev/null; true